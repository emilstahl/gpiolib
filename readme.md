#gpiolib for RPi
    startBoard()
    Sets the GPIO mode to Board.
    
    cleanGPIO()
    Cleans the settings for the GPIO pins.
    
    quickStart()
    Turns warnings off, cleans up and run startBoard().
    
    setIn(pin)
    Sets the defined pin as input.
    
    setPinsIn(pins[])
    Sets a list of pins as input.
    
    setOut(pin)
    Sets the defined pin as output.
    
    setPinsOut(pins[])
    Sets a list of pins as output.
    
    blinkLED(pin, time)
    Lights the defined pin for the number of seconds defined.